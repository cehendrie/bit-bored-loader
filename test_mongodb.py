# encoding: utf-8
import pymongo

mongo_client = pymongo.MongoClient('mongodb://localhost:27017/')
db = mongo_client['bit_bored']
bit_collection = db['bit']

bit = { 'scenario': 'scenario', 'details': 'Character: Says something.'}

b = bit_collection.insert_one(bit)

# for database in mongo_client.list_databases():
#     print(database)

bits = bit_collection.find()

for bit in bits:
    print(f"bit: {bit}")
