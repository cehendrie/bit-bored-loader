# encoding: utf-8
import os
import pymongo

from argparse import ArgumentParser

mongo_client = pymongo.MongoClient('mongodb://localhost:27017/')
db = mongo_client['bit_bored']
bit_collection = db['bit']

def arg_parser():
    argparser = ArgumentParser()
    argparser.add_argument('-f', '--file', required=True, help='a bit bored file')
    args = argparser.parse_args()
    return args

def to_bit(data):
    bit = {}
    for raw in data:
        if raw[:1] == '<':
            bit['scenario'] = raw.lstrip('<').rstrip('>').strip()
        elif raw[:1] == '-':
            if 'comments' in bit:
                bit['comments'].append(raw)
            else:
                bit['comments'] = [raw]
        else:
            if 'bit' in bit:
                bit['bit'].append(raw)
            else:
                bit['bit'] = [raw]
    return bit

def extract_bit_data(file):
    f = open(file)
    line = f.readline()
    bits = []
    extracted_bit = []
    while line:
        l = line.strip()
        if l[:5] == '-----':
            bits.append(to_bit(extracted_bit))
            extracted_bit = []
        elif len(l) > 0:
            extracted_bit.append(line.strip())
        line = f.readline()
    if len(extracted_bit) > 0:
        bits.append(to_bit(extracted_bit))
    f.close()
    return bits

def main():
    args = arg_parser()
    bits = extract_bit_data(args.file)
    bit_collection.insert_many(bits)
    print(f"*** successfully loaded bits: ({len(bits)})")

if __name__ == "__main__":
    main()
